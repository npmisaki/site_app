class CreateFaqs < ActiveRecord::Migration[5.2]
  def change
    create_table :faqs do |t|
      t.string :question
      t.string :answer
      t.references :faq_category
      t.integer :position, :null => false, :default => 1
      t.boolean :status, :default => true


      t.timestamps
    end
  end
end
