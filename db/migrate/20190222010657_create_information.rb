class CreateInformation < ActiveRecord::Migration[5.2]
  def change
    create_table :information do |t|
      t.string :date
      t.string :title
      t.string :content
      t.string :image
      t.integer :position, :null => false, :default => 1
      t.boolean :status, :default => true

      t.timestamps
    end
  end
end
