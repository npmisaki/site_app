class AddReferenceToInformation < ActiveRecord::Migration[5.2]
  def change
  	add_reference :information, :information_status, foreign_key: true
  end
end
