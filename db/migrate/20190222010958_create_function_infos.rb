class CreateFunctionInfos < ActiveRecord::Migration[5.2]
  def change
    create_table :function_infos do |t|
      t.string :title
      t.string :image
      t.string :description
      t.integer :position, :null => false, :default => 1
      t.boolean :status, :default => true

      t.timestamps
    end
  end
end
