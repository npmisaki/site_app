Rails.application.routes.draw do
  root :to => 'tops#index'
  get 'news/index'
  get 'news/show'
  get 'tops/faq'
  get 'tops/work'
  get 'tops/price'
  get 'tops/function'

  resources :contacts, :only => [:new, :create] do
    collection do
      post :preview
      get :thanks
    end
  end

  namespace :ar_admin do
    root :to => 'admin#index'
    resources :information_statuses, :except => [:new, :show]

    resources :informations do
      member do
        put :sort_up, :sort_down
      end
    end

    resources :faqs do
      member do
        put :sort_up, :sort_down
      end
    end    

    resources :faq_categories , :only => [:index, :new, :create, :edit, :update, :destroy] do
      member do
         put :sort_up, :sort_down
      end
    end

    # put 'faq_categories/sort_up'
    # put 'faq_categories/sort_down'

    resources :function_infos do
      member do
        put :sort_up, :sort_down
      end
    end

    resources :contacts, :only => [:index, :show] do
      collection do
        post :update_status
      end
    end

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
end