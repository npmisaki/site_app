// ボタンロールオーバー用
function rollOver(){
    var preLoad = new Object();
    $('img.over,input.over').each(function(){
        var imgSrc = this.src;
        var fType = imgSrc.substring(imgSrc.lastIndexOf('.'));
        var imgName = imgSrc.substr(0, imgSrc.lastIndexOf('.'));
        var imgOver = imgName + '-on' + fType;
        preLoad[this.src] = new Image();
        preLoad[this.src].src = imgOver;
        $(this).hover(
            function (){
                this.src = imgOver;
            },
            function (){
                this.src = imgSrc;
            }
        );
    });
}
$(document).ready(rollOver);


$(function() {

	// page link
	$('.pagelink').click(function(e) {
		e.preventDefault();
		var url = $(this).attr("href");

		var top = $(url).offset().top;
		
		$('html,body').animate({ scrollTop: (top-100) + "px" }, 'fast');

		var disp = $("#switch").css("display");
		if( disp != "none" ) {
			$('#switch').removeClass("active");	
			$("#navi ul").css("display","");
		}

	});


	// slider
	if(0 < $("#slider").size()){
		$('#slider').slick({
			dots: true,
			arrows: false,
			infinite: true,
			autoplay: true,
			autoplaySpeed: 6000,
			speed: 1000,
			fade: true,
			cssEase: 'linear'
		});
	}


	if(0 < $(".scroll").size()){
		rolling();
	}



	// faq
	$('.faq_link').children('a').click(function(e) {
		e.preventDefault();
		$(this).toggleClass('active');
		$(this).parent('dt').next('dd').slideToggle(500);
	});


	// navi
	$('#switch').click(function(e) {
		e.preventDefault();
		$(this).toggleClass("active");
		$("#navi ul").slideToggle(500);
	});


	$(window).scroll(function () {
		var s = $(this).scrollTop();
		if(s > 1) {
			$("#header").addClass('fixed');
		} else if(s < m) {
			$("#header").removeClass('fixed');
		}


        $('.slidein').each(function(){
            var elemPos = $(this).offset().top;
            var scroll = $(window).scrollTop();
            var windowHeight = $(window).height();
            if (scroll > elemPos - windowHeight + 100){
                $(this).addClass('on');
            }
        });

        $('.slidein_left').each(function(){
            var elemPos = $(this).offset().top;
            var scroll = $(window).scrollTop();
            var windowHeight = $(window).height();
            if (scroll > elemPos - windowHeight + 100){
                $(this).addClass('on');
            }
        });

        $('.slidein_right').each(function(){
            var elemPos = $(this).offset().top;
            var scroll = $(window).scrollTop();
            var windowHeight = $(window).height();
            if (scroll > elemPos - windowHeight + 100){
                $(this).addClass('on');
            }
        });
		
        $('.slidein_arrow01').each(function(){
            var elemPos = $(this).offset().top;
            var scroll = $(window).scrollTop();
            var windowHeight = $(window).height();
            if (scroll > elemPos - windowHeight + 100){
                $(this).addClass('on');
            }
        });
		
        $('.slidein_arrow02').each(function(){
            var elemPos = $(this).offset().top;
            var scroll = $(window).scrollTop();
            var windowHeight = $(window).height();
            if (scroll > elemPos - windowHeight + 100){
                $(this).addClass('on');
            }
        });

	});
	$("#pagetop").click(function () {
		$('html,body').animate({ scrollTop: 0 }, 'fast');
		return false;
	});

	if(0 < $(".fadein").size()){
		$(".fadein").animate({marginTop:"-20px",opacity:"1"},800);
	}

	if(0 < $(".parts").size()){
		var length = $("input.text").length;
		for ( var i = 0; i < length; i++ ) {

			if( $("input.text").eq(i).val() != "" ) {
				$("input.text").eq(i).parent(".parts").children("label").addClass("active");
			}

		}

		var length2 = $(".select").length;
		for ( var i = 0; i < length2; i++ ) {

			if( $(".select").eq(i).children("select").val() != "" ) {
				$(".select").eq(i).parent(".parts").children("label").addClass("active");
				$(".select").eq(i).children("select").removeClass("none_active");
			}

		}



		$("input.text").focus(function(){
			$(this).parent(".parts").children("label").addClass("active");
		}).blur(function(){
			var length = $("input.text").length;
			for ( var i = 0; i < length; i++ ) {
	
				if( $("input.text").eq(i).val() != "" ) {
					$("input.text").eq(i).parent(".parts").children("label").addClass("active");
				} else {
					$("input.text").eq(i).parent(".parts").children("label").removeClass("active");
				}
	
			}
		});

		$(".select").children("select").focus(function(){
			$(this).removeClass("none_active");
			$(this).parent(".select").parent(".parts").children("label").addClass("active");
		}).blur(function(){
			if( $(this).val() != "" ) {
				
			} else {
				$(this).addClass("none_active");
			$(this).parent(".select").parent(".parts").children("label").removeClass("active");
			}
		});

		 
	}

});

$(window).resize(function() {
	var disp = $("#switch").css("display");
	if( disp == "none" ) {
		$('#switch').removeClass("active");	
		$("#navi ul").css("display","");
	}
});


function rolling() {
	$(".scroll_line").children("img").animate({top: "100%"},1500,null,function() {
		$(".scroll_line").children("img").css("top","-100%");
		rolling();
	});
}
