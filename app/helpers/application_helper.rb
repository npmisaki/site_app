module ApplicationHelper
  def error_on(object, method, label_name = nil)
    obj = instance_variable_get("@#{object}")
    attribute_name = label_name.presence || obj.class.human_attribute_name(method.to_s)
    errors = obj.errors[method].present? ? obj.errors[method] : nil
    if errors
      return content_tag("p", content_tag("em", "#{attribute_name}#{errors.is_a?(Array) ? errors.first : errors}"), :class => "error")
    end
    nil
  end

  
  # 一覧表示のソート用リンクを生成する
  # :with_params => true が指定された場合は パラメータを保持する
  def sort_link(column, options = {})
    direction = (column.to_s == params[:sort] && params[:direction] == 'asc') ? 'desc' : 'asc'
    sort_mark = direction == 'asc' ? '▼' : '▲'
    sort_params = if options[:with_params]
                    params.merge({:sort => column, :direction => direction})
                  else
                    {:sort => column, :direction => direction}
                  end
    link_to sort_mark, sort_params
  end


end
