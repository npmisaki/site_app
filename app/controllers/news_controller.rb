class NewsController < ApplicationController
  def index
    @informations = Information.all
  end

  def show
  	@informations = Information.all.limit(3)
  	@information = Information.find(params[:id])

  end
end