class ContactsController < ApplicationController


  def new
  	@contact = Contact.new
    render :action => "new"
  end

  def preview
  	@contact = Contact.new(contact_params)
    if @contact.valid?
      render :action => "preview"
    else
      flash.now[:warning] = "※プライバシーポリシーに同意してください。" if params[:contact][:terms].nil?
      render :action => "new"
    end

    session[:contact] = @contact
  end


  def create
    @contact = session[:contact]
    # 戻って修正
    if params['new.x'].present? || params['new'].present?
      render :action => "new"
      return
    end

    if @contact.save
      session[:contact] = nil
      redirect_to thanks_contacts_path
    else
      render :action => "new"
    end
  end

  def thanks
  	return thanks_contacts_path
  end

   private

  # Never trust parameters from the scary internet, only allow the white list through.
  def contact_params
    params.require(:contact).permit(:name, :name_kana, :email, :tel, :message, :terms)
  end
end
