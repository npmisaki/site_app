class ArAdmin::FunctionInfosController < ApplicationController
  after_action :sort_position, :only => [:create,:update,:destroy]

  def index
    @function_infos = FunctionInfo.order(:position)
    @function_info = FunctionInfo.new
  end

  def show
    @function_info = FunctionInfo.find(params[:id])
  end

  def create
    @function_info = FunctionInfo.new(function_info_params)

    if @function_info.save
      redirect_to ar_admin_function_infos_path, :notice => '機能を作成しました。'
    else
      render :action => "index"
    end
  end

  def edit
    @function_info = FunctionInfo.find(params[:id])
  end

  def update
    @function_info = FunctionInfo.find(params[:id])
    @function_info.attributes = (function_info_params)
    session[:function_info] = @function_info
    if @function_info.save
      redirect_to ar_admin_function_infos_path, :notice => '機能を更新しました。'
    else
      render :action => "edit"
    end
  end

  def destroy
    @function_info = FunctionInfo.find(params[:id])
    @function_info.destroy
    redirect_to ar_admin_function_infos_path, :notice => '機能を削除しました。'
  end

  def sort_up
    info = FunctionInfo.find(params[:id])
    target = FunctionInfo.order(:position).where("position < ?", info.position).last

    if target
      info_position, target_position = info.position, target.position
      target.update_attribute(:position, info_position)
      info.update_attribute(:position, target_position)
    end

    redirect_to request.env["HTTP_REFERER"] || ar_admin_function_infos_path
  end

  def sort_down
    info = FunctionInfo.find(params[:id])

    target = FunctionInfo.order(:position).where("position > ?", info.position).first

    if target
      info_position, target_position = info.position, target.position
      target.update_attribute(:position, info_position)
      info.update_attribute(:position, target_position)
    end
    redirect_to request.env["HTTP_REFERER"] || ar_admin_function_infos_path
  end

  private
  def sort_position
    position = 1
    FunctionInfo.order(:position,"updated_at DESC").each do |record|
        record.update_attribute(:position,position)
      position += 1
    end
  end

  def function_info_params
    params.require(:function_info).permit(:title, :image, :description, :position, :status)
  end
end
