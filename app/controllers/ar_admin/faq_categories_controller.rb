class ArAdmin::FaqCategoriesController < ApplicationController

  def index
    @faq_category = FaqCategory.new
    list_common
    @faq_categories = FaqCategory.order(:position)
  end
  
  def create
    @faq_category = FaqCategory.new(faq_category_params)   

    position = FaqCategory.maximum(:position)
    position = position.to_i + 1
    @faq_category.position = position
    
    if @faq_category.save
      redirect_to({:action => "index"}, :notice => 'よくある質問 カテゴリーを作成しました。')
    else
      list_common
      render :action => "index" 
    end
  end
  
  def edit
    @faq_category = FaqCategory.find(params[:id])
  end
  
  def update
    @faq_category = FaqCategory.find(params[:id])
    @faq_category.attributes = (faq_category_params) 
    
    if @faq_category.save
      redirect_to({:action => "index"}, :notice => 'よくある質問 カテゴリーを更新しました。')
    else
      render :action => "edit" 
    end
  end
  
  def destroy
    @faq_category = FaqCategory.find(params[:id])
    # site_id = @faq_category.site_id
    @faq_category.destroy
    FaqCategory.reset_group_position
    
    redirect_to({:action => "index"}, :notice => 'よくある質問 カテゴリーを削除しました。')
  end
  
  def sort_up
    base = FaqCategory.find(params[:id])
    
    target = FaqCategory.order(:position).where("position < ?", base.position).last
    
    if target
      base_position, target_position = base.position, target.position
      target.update_attribute(:position, base_position)
      base.update_attribute(:position, target_position)
      
    end
    
    redirect_to request.env["HTTP_REFERER"] || ar_admin_faq_category_index_path
  end
  
  def sort_down
    base = FaqCategory.find(params[:id])
    
    target = FaqCategory.order(:position).where("position > ?", base.position).first
    
    if target
      base_position, target_position = base.position, target.position
      target.update_attribute(:position, base_position)
      base.update_attribute(:position, target_position)
    end
    
    redirect_to request.env["HTTP_REFERER"] || ar_admin_faq_category_index_path
  end
  
  private
  
  # 一覧の共通処理
  def list_common
    @faq_categories = FaqCategory.order(:position)
  end

  def faq_category_params
    params.require(:faq_category).permit(:name, :position)
  end
end
