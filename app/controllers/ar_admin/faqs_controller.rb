class ArAdmin::FaqsController < ApplicationController
  protect_from_forgery :except => [:update]
  def index
    @faq = Faq.new
    edit_common
    list_common
  end
  
  def create
    @faq = Faq.new(faq_params)
    
    if @faq.save
      redirect_to({:action => "index"}, :notice => 'よくある質問を作成しました。')
      Faq.reset_group_position @faq.faq_category_id
    else
      edit_common
      list_common
      render :action => "index" 
    end
  end
  
  def edit
    @faq = Faq.find(params[:id])
    edit_common
  end

  def update
    edit_common
    @faq = Faq.find(params[:id])
    @faq.attributes = (faq_params)
    
    if @faq.save
      redirect_to({:action => "index"}, :notice => 'よくある質問を更新しました。')
      Faq.reset_group_position @faq.faq_category_id      
    else
      render :action => "edit" 
    end
  end
  
  def destroy
    @faq = Faq.find(params[:id])
    faq_category_id = @faq.faq_category_id
    @faq.destroy
    Faq.reset_group_position faq_category_id
    
    redirect_to({:action => "index"}, :notice => 'よくある質問を削除しました。')
  end
  
  def sort_up
    base = Faq.find(params[:id])
    
    target = Faq.order(:position).where(:faq_category_id => params[:faq_category_id]).
             where("position < ?", base.position).last
    
    if target
      base_position, target_position = base.position, target.position
      target.update_attribute(:position, base_position)
      base.update_attribute(:position, target_position)
      
    end
    
    redirect_to request.env["HTTP_REFERER"] || ar_admin_faq_index_path
  end
  
  def sort_down
    base = Faq.find(params[:id])
    
    target = Faq.order(:position).where(:faq_category_id => params[:faq_category_id]).
             where("position > ?", base.position).first
    
    if target
      base_position, target_position = base.position, target.position
      target.update_attribute(:position, base_position)
      base.update_attribute(:position, target_position)
    end
    
    redirect_to request.env["HTTP_REFERER"] || ar_admin_faq_index_path
  end
  
  private
  
  def edit_common
    @faq_categories = FaqCategory.order(:position)
  end
  
  def list_common
    @faqs = Faq.order("faq_categories.position, faqs.position")
    @faqs = @faqs.includes(:faq_category)
  end
  
  def faq_params
    params.require(:faq).permit(:question, :answer,:posotion, :faq_category_id)
  end
end

