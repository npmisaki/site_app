class ArAdmin::InformationStatusesController < ApplicationController
  def index
    @information_statuses = InformationStatus.all
    @information_status = InformationStatus.new
  end

  def create
    @information_status = InformationStatus.new(information_status_params)
    
    if @information_status.save
      redirect_to [:ar_admin, :information_statuses], :notice => 'お知らせステータスを登録しました。'
    else
      @information_statuses = InformationStatus.all
      render :action => "index" 
    end
  end
  
  def edit
    @information_status = InformationStatus.find(params[:id])
  end
  
  def update
    @information_status = InformationStatus.find(params[:id])
    @information_status.attributes = (information_status_params)
    
    if @information_status.save
      redirect_to [:ar_admin, :information_statuses], :notice => 'お知らせステータスを更新しました。'
    else
      render :action => "edit"
    end
  end
  
  def destroy
    @information_status = InformationStatus.find(params[:id])
    @information_status.destroy
    
    redirect_to [:ar_admin, :information_statuses], :notice => 'お知らせステータスを削除しました。'
  end

  private

  def information_status_params
    params.require(:information_status).permit(:name, :image)
  end
end

