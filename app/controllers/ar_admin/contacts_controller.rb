class ArAdmin::ContactsController < ApplicationController
  helper_method :sort_column, :sort_direction 
  
  def index
    @contacts = Contact.order(sort_column + " " + sort_direction)
  end
  
  def show
    @contact = Contact.find(params[:id])
  end

private

  def sort_direction
    %w[asc desc].include?(params[:direction]) ?  params[:direction] : "asc"
  end

  def sort_column
      Contact.column_names.include?(params[:sort]) ? params[:sort] : "id"
  end

end
