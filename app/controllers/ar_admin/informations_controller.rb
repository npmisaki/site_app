class ArAdmin::InformationsController < ApplicationController
  after_action :sort_position, :only => [:create,:update,:destroy]

  def index
    @informations = Information.order(:position)
    @information = Information.new
    @information_statuses = InformationStatus.all
    # @selected_status_ids = @information.information_statuses.map(&:id)
  end

  def create
    @information = Information.new(information_params)
    if @information.save
      redirect_to ar_admin_informations_path, :notice => 'お知らせを作成しました。'
      session[:information] = nil
    else
      render :action => "index"
    end
  end

  def edit
    @information = Information.find(params[:id])
  end

  def update
    @information = Information.find(params[:id])
    @information.attributes = params[:information]

    if @information.save
      redirect_to ar_admin_informations_path, :notice => 'お知らせを更新しました。'
      session[:information] = nil
    else
      render :action => "edit"
    end
  end

  def destroy
    @information = Information.find(params[:id])
    @information.destroy

    redirect_to ar_admin_informations_path :notice => 'お知らせを削除しました。'
  end

  def sort_up
    info = Information.find(params[:id])
    target = Information.order(:position).where("position < ?", info.position).last

    if target
      info_position, target_position = info.position, target.position
      target.update_attribute(:position, info_position)
      info.update_attribute(:position, target_position)
    end

    redirect_to request.env["HTTP_REFERER"] || ar_admin_information_index_path
  end

  def sort_down
    info = Information.find(params[:id])
    target = Information.order(:position).where("position > ?", info.position).first

    if target
      info_position, target_position = info.position, target.position
      target.update_attribute(:position, info_position)
      info.update_attribute(:position, target_position)
    end

    redirect_to request.env["HTTP_REFERER"] || ar_admin_information_index_path
  end

  private

  def sort_position
    position = 1
    Information.order(:position,"updated_at DESC").each do |record|
        record.update_attribute(:position,position)
      position += 1
    end
  end

  def information_params
    params.require(:information).permit( :date, :title, :content, :image, :information_status_id)
  end
end