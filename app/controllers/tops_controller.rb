class TopsController < ApplicationController
  def index
    @information = Information.all.order(created_at: :desc).limit(3)
  end

  def faq
    @faqs = FaqCategory.order("faq_categories.position, faqs.position").includes(:faqs).where("faqs.status" => true)
  end

  def price
  end

  def function
     @function_infos = FunctionInfo.all.order(:position)
  end
end
