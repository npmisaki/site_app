class Faq < ApplicationRecord
  belongs_to :faq_category
  scope :faq_category, lambda{|faq_category_id| where "faq_category_id = ?", faq_category_id}

  def self.reset_group_position(faq_category_id)
    position = 1
    Faq.order(:position).faq_category(faq_category_id).each do |faq|
      faq.position = position
      faq.save
      position += 1
    end
  end
end
