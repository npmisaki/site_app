class Information < ApplicationRecord
  mount_uploader :image, ImageUploader
  paginates_per 6
  # attr_accessor :date_period_from, :hour_period_from, :min_period_from,
  #               :date_period_to, :hour_period_to, :min_period_to

  # def get_period
  #   self.date_period_from = period_from.to_date.strftime('%Y-%m-%d') if period_from.present?
  #   self.hour_period_from = "%02d" % period_from.hour if period_from.present?
  #   self.min_period_from  = "%02d" % period_from.min if period_from.present?

  #   self.date_period_to = period_to.to_date.strftime('%Y-%m-%d') if period_to.present?
  #   self.hour_period_to = "%02d" % period_to.hour if period_to.present?
  #   self.min_period_to  = "%02d" % period_to.min if period_to.present?
  # end

  # def set_period
  #   self.period_from = "#{date_period_from} #{hour_period_from}:#{min_period_from}"
  #   self.period_to   = "#{date_period_to} #{hour_period_to}:#{min_period_to}"
  # end
end
