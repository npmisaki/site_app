class InformationStatus < ApplicationRecord
  
  validates :name,
            :length => {:maximum => 30},
            :presence => true
  
  mount_uploader :image, ImageUploader
end
