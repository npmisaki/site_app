class Contact < ApplicationRecord
  validates :name, :name_kana,
            :presence => true,
            :length => { :maximum => 60 }

  validates :email,
            :presence => true,
            :format => { :with => /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/ }

  validates :tel,
            :presence => true

  validates :message,
            :presence => true

  validates :terms,
            :presence => true,
            :acceptance => true
end
